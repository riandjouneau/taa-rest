Projet TAA de Riand Morgan & Jouneau Nicolas, M2 Mitic.

Ce projet contient une implémentation JPA permettant le travail sur une base de données représentée par des objets Java.
Cette implémentation fonctionne via une API Rest, qu'il est possible d'accéder via Swagger en localhost.

Pour lancer le projet :

1. Lancer en java RestApplication.java
2. Consultez swagger sur localhost:8080/api

Pour consulter la base de donnée et la tester : 

1. Allez sur http://phpmyadmin2.istic.univ-rennes1.fr/ avec pseudo user_14009270 et mot de passe 123
2. Test sur swagger

Note : les id sont rajoutées automatiquement, pour faire une requête post (add), il faut laisser l'id a 0.