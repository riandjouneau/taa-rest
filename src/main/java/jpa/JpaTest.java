package jpa;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import domain.*;

public class JpaTest {
	private EntityManager manager;
	
	public JpaTest(EntityManager manager){
		this.manager = manager;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("mysql");
		EntityManager manager = factory.createEntityManager();
		JpaTest test = new JpaTest(manager);

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			test.createAndFillProject(manager);

		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		manager.close();
		factory.close();
	}

	private void createAndFillProject(EntityManager manager) {
		Project p = new Project("proj_test",new Date(2015,12,12));
		Version v1 = new Version("0.1",p.getReleaseDate(),p);
		Sprint s1 = new Sprint("sprint 1", new Date(), v1.getReleaseDate(), v1);
		Epic e1 = new Epic("epic 1", 8, s1);
		Story st1 = new Story("story 1",1,e1);
		Task t1 = new Task("User will connect",st1);
		Developer dev1 = new Developer("Dev1",t1);
		Developer dev2 = new Developer("Dev2",t1);
		
		
//		Version v2 = new Version("0.2",new Date(2015,11,11),project);
//		Sprint s1 = new Sprint("sprint 1", new Date(), new Date(2015,10,1), v1);
//		Sprint s2 = new Sprint("sprint 2", s1.getToDate(), v1.getReleaseDate(), v1);
//		Sprint s3 = new Sprint("sprint 3", v1.getReleaseDate(), new Date(2015,10,1), v2);
//		Sprint s4 = new Sprint("sprint 4", s3.getToDate(), v2.getReleaseDate(), v2);
//		Epic e1 = new Epic("1 epic",1,s1);
//		Epic e2 = new Epic("2 epic",2,s2);
//		Epic e3 = new Epic("3 epic",3,s3);
//		Epic e4 = new Epic("4 epic",4,s4);
//		Story st1 = new Story("story 1",1,e1);
		
		
		
		manager.persist(p);
		manager.persist(v1);
		manager.persist(s1);
		manager.persist(e1);
		manager.persist(st1);
		manager.persist(t1);
		manager.persist(dev1);
		manager.persist(dev2);
		
		showAllDB(manager);
		
	}
	
	private void showAllDB(EntityManager manager){
		//List<Developer> resultList = manager.createQuery("Select a From Developer a",Developer.class).getResultList();
		//System.out.println("Test developer "+resultList); 
		
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		// assuming a is an Integer  
		// if returning multiple fields, look into using a Tuple 
		// or specifying the return type as an Object or Object[]
		CriteriaQuery<String> criteriaQuery = criteriaBuilder.createQuery(String.class);
		Root<Developer> from = criteriaQuery.from(Developer.class);
		criteriaQuery.multiselect(from.get("name"))
		.where(from.get("a").in(1, 2, 3, 4));
		TypedQuery<String> query = manager.createQuery(criteriaQuery);
		System.out.println("Query test : "+query.getResultList());
	}

}
