package repository;

import domain.Developer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class DeveloperRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
    EntityManager manager = factory.createEntityManager();
    EntityTransaction tx = manager.getTransaction();

    public Developer findOneByName(String name) {
    	tx.begin();
        Optional<Developer> dev = Optional.empty();
        try {
        	dev = Optional.of(manager.createQuery("SELECT e from Developer e WHERE e.name = :name", Developer.class)
                    .setParameter("name", name)
                    .getSingleResult());

        } catch (Exception e) {
            
        }
        tx.commit();
        System.out.println(dev);
        if(dev.isPresent()){
        	return dev.get();
        }
        return null;
    }
    
    public Developer findOneById(Long id) {
    	tx.begin();
        Optional<Developer> dev = Optional.empty();
        try {
        	dev = Optional.of(manager.createQuery("SELECT e from Developer e WHERE e.id = :id", Developer.class)
                    .setParameter("id", id)
                    .getSingleResult());

        } catch (Exception e) {
            
        }
        tx.commit();
        System.out.println(dev);
        if(dev.isPresent()){
        	return dev.get();
        }
        return null;
    }
    
    public void add(Developer dev) {
    	tx.begin();
		manager.persist(dev);
		tx.commit();
    }
}
