package repository;

import domain.Developer;
import domain.Task;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
    EntityManager manager = factory.createEntityManager();
    EntityTransaction tx = manager.getTransaction();

    public List<Task> findAll() {
        tx.begin();
        List<Task> Tasks = new ArrayList<Task>();
        try {
            Tasks = manager.createQuery("SELECT d from Task d", Task.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return Tasks;
    }
    
    public void add(Task task) {
    	tx.begin();
		manager.persist(task);
		tx.commit();
    }
}
