package domain;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Project {
	@Id
	@GeneratedValue
	private long id;
	private String name;
	private Date releaseDate;
	@OneToMany(mappedBy="project")
	private List<Version> versions;
	
	public Project() {
	}
	
	public Project(String name, Date releaseDate) {
		this.name = name;
		this.releaseDate = releaseDate;
		versions = new ArrayList<Version>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public List<Version> getVersions() {
		return versions;
	}
	public void setVersions(List<Version> versions) {
		this.versions = versions;
	}
	public void addVersion(Version version){
		versions.add(version);
	}
}
