package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Story {
	@Id
	@GeneratedValue
	private long id;
	private String text;
	private int priority;
	@OneToMany(mappedBy="story")
	private List <Task> tasks;
	@ManyToOne
	private Epic epic;
	
	public Story() {
	}

	public Story(String text, int priority, Epic epic) {
		epic.addStory(this);
		this.text = text;
		this.priority = priority;
		this.epic = epic;
		tasks = new ArrayList<Task>();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	public void addTasks(Task task){
		tasks.add(task);
	}
	public Epic getEpic() {
		return epic;
	}
	public void setEpic(Epic Epic) {
		this.epic = Epic;
	}
}
