package domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;



@Entity
public class Developer {
	@Id
	@GeneratedValue
	private long id;
	private String name;
	@ManyToMany
	@JsonIgnore
	private List<Task> tasks;
	
	public Developer() {
	}
	
	public Developer(String name, Task task) {
		this.name = name;
		tasks = new ArrayList<Task>();
		addTask(task);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	
	@JsonIgnore
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	public void addTask(Task task){
		tasks.add(task);
		task.addDeveloper(this);
	}
}
