package domain;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Sprint {
	@Id
	@GeneratedValue
	private long id;
	private String name;
	private Date fromDate;
	private Date toDate;
	@OneToMany(mappedBy="sprint")
	private List <Epic> epics;
	@ManyToOne
	private Version version;
	
	public Sprint() {
	}
	
	public Sprint(String name, Date fromDate, Date toDate, Version version) {
		version.addSprint(this);
		this.name = name;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.version = version;
		epics = new ArrayList<Epic>();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public List<Epic> getEpics() {
		return epics;
	}
	public void setEpics(List<Epic> epics) {
		this.epics = epics;
	}
	public void addEpics(Epic epic){
		epics.add(epic);
	}
	public Version getVersion() {
		return version;
	}
	public void setVersion(Version version) {
		this.version = version;
	}
}
