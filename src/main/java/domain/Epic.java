package domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
public class Epic {
	@Id
	@GeneratedValue
	private long id;
	private String text;
	private int priority;
	@OneToMany(mappedBy="epic")
	private List<Story> stories;
	@ManyToOne
	private Sprint sprint;
	
	public Epic() {
	}
	
	public Epic(String text, int priority, Sprint sprint) {
		sprint.addEpics(this);
		this.text = text;
		this.priority = priority;
		this.sprint = sprint;
		stories = new ArrayList<Story>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public List<Story> getStories() {
		return stories;
	}
	public void setStories(List<Story> stories) {
		this.stories = stories;
	}
	public void addStory(Story story){
		stories.add(story);
	}
	public Sprint getSprint() {
		return sprint;
	}
	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}
}
