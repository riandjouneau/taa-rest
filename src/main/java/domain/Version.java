package domain;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Version {
	@Id
	@GeneratedValue
	private long id;
	private String number;
	private Date releaseDate;
	@OneToMany(mappedBy="version")
	private List<Sprint> sprints;
	@ManyToOne
	private Project project;
	
	public Version() {
	}
	
	public Version(String number, Date releaseDate, Project project) {
		project.addVersion(this);
		this.number = number;
		this.releaseDate = releaseDate;
		this.project = project;
		sprints = new ArrayList<Sprint>();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public List<Sprint> getSprints() {
		return sprints;
	}
	public void setSprints(List<Sprint> sprints) {
		this.sprints = sprints;
	}
	public void addSprint(Sprint sprint){
		sprints.add(sprint);
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
}
