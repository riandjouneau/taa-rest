package domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
public class Task {
	@Id
	@GeneratedValue
	private long id;
	private String text;
	@ManyToOne
	private Story story;
	private Boolean done;
	private int estimate;
	@ManyToMany
	private List <Developer> developers;
	
	public Task() {
	}
	
	public Task(String text, Story story) {
		done = false;
		this.story = story;
		this.text = text;
		developers = new ArrayList<Developer>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Story getStory() {
		return story;
	}
	public void setStory(Story story) {
		this.story = story;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Boolean getDone() {
		return done;
	}
	public void setDone(Boolean done) {
		this.done = done;
	}
	public int getEstimate() {
		return estimate;
	}
	public void setEstimate(int estimate) {
		this.estimate = estimate;
	}
	public List <Developer> getDevelopers() {
		return developers;
	}
	public void setDevelopers(List<Developer> developers) {
		this.developers = developers;
	}
	public void addDeveloper(Developer developer){
		developers.add(developer);
	}
}
