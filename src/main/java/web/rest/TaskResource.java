package web.rest;

import domain.Task;
import io.swagger.annotations.Api;
import repository.TaskRepository;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/task")
@Api(value="/task", description = "Task resource")
public class TaskResource {

    private TaskRepository taskRepository = new TaskRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> getAll() {
        return taskRepository.findAll();
    }
    
    @POST
    @Path(value = "/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void add(Task task) {
    	System.out.println(task);
    	taskRepository.add(task);
    }
}
