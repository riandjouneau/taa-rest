package web.rest;

import io.swagger.annotations.Api;
import repository.DeveloperRepository;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Developer;

@Path("/developer")
@Api(value="/developer", description = "Developer resource")
public class DeveloperResource {

    private DeveloperRepository developerRepository = new DeveloperRepository();

    @GET
    @Path(value = "{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Developer getByName(@PathParam("name") String name) {
    	return developerRepository.findOneByName(name);
    }
    
    @GET
    @Path(value = "{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Developer getById(@PathParam("id") Long id) {
    	return developerRepository.findOneById(id);
    }
    
    @POST
    @Path(value = "/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void add(Developer dev) {
    	System.out.println(dev);
    	developerRepository.add(dev);
    }
}
